package myApplication.dao;


import myApplication.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserDAO extends CrudRepository<User, Integer> {


    default User updateUser(Integer id, User user) {
        Optional<User> userToUpdate = findById(id);
        if (userToUpdate.isPresent()) {
            User userWithIdFound = userToUpdate.get();
            userWithIdFound.setEmail(user.getEmail());
            userWithIdFound.setRole(user.getRole());
            userWithIdFound.setPhoneNumber(user.getPhoneNumber());
            return save(userWithIdFound);
        }
        return save(user);
    }
}
