package myApplication.controller;

import myApplication.entity.User;
import myApplication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    @Autowired
    UserService userService;


    @PostMapping("/insertUser")
    public @ResponseBody
    User insertUser(@RequestBody User user) {
        return userService.insertUser(user);
    }
        @GetMapping("/getAllUsers")
        public @ResponseBody Iterable<User> getAllUsers () {
            return userService.getAllUsers();
        }

    @DeleteMapping("/deleteUser/{id}")
    public void deleteUser(@PathVariable Integer id) {
        userService.deleteUserById(id);
    }
    @PutMapping("/updateUser/{id}")
    public @ResponseBody User updateUser(@PathVariable Integer id, @RequestBody User user){
        userService.updateUser(id, user);
        return user;
    }
    @GetMapping("/getUsersByRole/{role}")
    public @ResponseBody Iterable<User> getUsersByRole(@PathVariable String role) {
        return userService.getUsersByRole(role);
    }

}
