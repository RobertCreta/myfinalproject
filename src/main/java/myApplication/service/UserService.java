package myApplication.service;

import myApplication.dao.UserDAO;
import myApplication.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    @Autowired

    UserDAO userDAO;

    public User insertUser(User user) {
        return userDAO.save(user);
    }

    public void deleteUserById(Integer id) {
        userDAO.deleteById(id);

    }

    public void updateUser(Integer id, User user) {
        userDAO.updateUser(id, user);
    }

    public Iterable<User> getUsersByRole(String role) {

        Iterable<User> all = userDAO.findAll();
        List<User> userList = new ArrayList<>();
        for (User user : all) {
            if (user.getRole().equals(role)) {
                userList.add(user);
            }
        }
        return userList;

    }


    public Iterable<User> getAllUsers() {
            return userDAO.findAll();
        }
    }

