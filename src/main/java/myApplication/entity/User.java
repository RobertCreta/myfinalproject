package myApplication.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Getter
private Integer id;
@Getter
@Setter
    private String role;
@Getter
@Setter
    private String name;
@Getter
@Setter
    private Integer phoneNumber;
@Getter
@Setter
   private String email;

}
